#!/usr/bin/env bash

(cd ~/spec-mock && git checkout . && git pull)
(cd ~/quick-mock && git checkout . && git pull)
(cd ~/interface-mock-akka && git checkout . && git pull)

(cd ~/spec-mock && sbt clean assembly)
(cd ~/quick-mock && sbt clean assembly)

yes | cp ~/application.conf ~/interface-mock-akka/src/main/resources
yes | cp ~/package.json ~/interface-mock-akka/src/main/resources/front-end
(cd ~/interface-mock-akka/src/main/resources/front-end && npm install)
(cd ~/interface-mock-akka/src/main/resources/front-end && npm run build-prod)
(cd ~/interface-mock-akka && sbt clean assembly)