package com.dexter.smock.tests

import akka.http.javadsl.model.ContentTypes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.dexter.smock.Launch
import com.dexter.smock.db.SMRedisClient
import com.dexter.smock.model.Scenario
import com.dexter.smock.rest.SpeckMockController
import io.circe.syntax._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpec}
import org.slf4j.{Logger, LoggerFactory}

class ValidatorTests extends WordSpec with Matchers with ScalatestRouteTest with MockitoSugar {

  final private val LOG: Logger = LoggerFactory.getLogger(Launch.getClass)

  final private val redis: SMRedisClient = mock[SMRedisClient]
  final private val controller = new SpeckMockController(redis)

  final private val specId = "testSpecId2"
  final private val blankScenarioId = ""

  final private val blankScenario = Scenario(
    blankScenarioId,
    "GET",
    List("segment1", "segment2"),
    Map("param1" -> "paramValue1", "param2" -> "paramValue2"),
    Map("headerParam1" -> "headerValue1", "headerParam2" -> "headerValue2"),
    Some("REQUEST BODY"),
    Some("RESPONSE BODY")
  )
  final private val wrongMethodScenario = Scenario(
    "someNewID3",
    "wrong method",
    List("segment1", "segment2"),
    Map("param1" -> "paramValue1", "param2" -> "paramValue2"),
    Map("headerParam1" -> "headerValue1", "headerParam2" -> "headerValue2"),
    Some("REQUEST BODY"),
    Some("RESPONSE BODY")
  )

  "Test Validator" should {
    "Blank scenarioId" in {
      Put("/sm/" + specId + "/someNewID")
        .withEntity(ContentTypes.APPLICATION_JSON, blankScenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual "incorrect scenarioId"
      }
    }
    "Wrong method" in {
      Put("/sm/" + specId + "/someNewID3")
        .withEntity(ContentTypes.APPLICATION_JSON, wrongMethodScenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual "incorrect method"
      }
    }
  }

}
