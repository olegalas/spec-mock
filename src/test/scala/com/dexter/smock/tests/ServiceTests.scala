package com.dexter.smock.tests

import akka.http.javadsl.model.ContentTypes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.util.ByteString
import com.dexter.smock.Launch
import com.dexter.smock.db.SMRedisClient
import com.dexter.smock.model.{Credentials, Scenario, QuickMockSpecification => Spec}
import com.dexter.smock.rest.SpeckMockController
import io.circe.syntax._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{Matchers, WordSpec}
import org.slf4j.{Logger, LoggerFactory}
import org.mockito.Mockito._
import org.mockito.Matchers.{any, eq => eqq}

import scala.concurrent.Future

class ServiceTests extends WordSpec with Matchers with ScalatestRouteTest with MockitoSugar {

  final private val LOG: Logger = LoggerFactory.getLogger(Launch.getClass)

  final private val redis: SMRedisClient = mock[SMRedisClient]
  final private val controller = new SpeckMockController(redis)

  final private val specId = "testSpecId"
  final private val userTokenId = "userTokenId"
  final private val specIdForPut = "testSpecIdForPut"
  final private val scenarioId = "testScenarioId"
  final private val newPass = "123"
  final private val someWrongId = "someWrongId"

  final private val scenario = Scenario(
    scenarioId,
    "GET",
    List("segment1", "segment2"),
    Map("param1" -> "paramValue1", "param2" -> "paramValue2"),
    Map("headerParam1" -> "headerValue1", "headerParam2" -> "headerValue2"),
    Some("REQUEST BODY"),
    Some("RESPONSE BODY")
  )

  final private val emptyScenario = Scenario(
    scenarioId,
    "GET",
    List(),
    Map(),
    Map(),
    None,
    None
  )

  final private val spec = Spec(specId, userTokenId,List(scenario))

  when(redis.>(any(), any())).thenReturn(Future.successful(Right(true)))
  when(redis.!(any())).thenReturn(Future.successful(Right(1)), Future.successful(Right(1)))
  when(redis.<(eqq(specId))).thenReturn(Future.successful(Right(Some{
    ByteString(spec.asJson.toString)
  })))
  when(redis.<(eqq(someWrongId))).thenReturn(Future.successful(Right(None)))
  when(redis.<(eqq(specIdForPut))).thenReturn(Future.successful(Right(None)))

  "Test SpecificationService" should {

    "GET. All spec be specId. Unknown specId" in {
      Get("/sm/" + someWrongId) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"Unknown specId : $someWrongId"
      }
    }
    "GET. All spec be specId. Was found spec" in {
      Get("/sm/" + specId) ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[Spec] shouldEqual spec
      }
    }

    "PUT. Create new SpecId. Blank specId" in {
      Put("/sm/" + "")
        .withEntity(ContentTypes.APPLICATION_JSON, newPass) ~> controller.route ~> check {
        handled shouldBe false
      }
    }
    "PUT. Create new SpecId. There already was QuickMockSpecification" in {
      Put("/sm/" + specId)
        .withEntity(ContentTypes.APPLICATION_JSON, Credentials("").asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"There already was QuickMockSpecification by : $specId"
      }
    }
    "PUT. Create new SpecId. New spec was successfully persisted" in {
      Put("/sm/" + specIdForPut)
        .withEntity(ContentTypes.APPLICATION_JSON, Credentials("123").asJson.toString)~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual "New spec was successfully persisted"
      }
    }

    "GET. Scenario by specId. Unknown specId" in {
      Get("/sm/" + someWrongId + "/" + scenarioId) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"Unknown specId : $someWrongId"
      }
    }
    "GET. Scenario by specId. Unknown scenarioId" in {
      Get("/sm/" + specId + "/" + someWrongId) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"Unknown scenarioId : $someWrongId"
      }
    }
    "GET. Scenario by specId. Was found scenario" in {
      Get("/sm/" + specId + "/" + scenarioId) ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[Scenario] shouldEqual scenario
      }
    }

    "POST. Update scenario. Unknown specId" in {
      Post("/sm/" + someWrongId + "/" + scenarioId)
        .withEntity(ContentTypes.APPLICATION_JSON, scenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"Unknown specId : $someWrongId"
      }
    }
    "POST. Update scenario. Unknown scenarioId" in {
      Post("/sm/" + specId + "/" + someWrongId)
        .withEntity(ContentTypes.APPLICATION_JSON, scenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"Unknown scenarioId (if you want create new scenario try PUT instead of POST) : $someWrongId"
      }
    }
    "POST. Update scenario. Was successfully updated scenario" in {
      Post("/sm/" + specId + "/" + scenarioId)
        .withEntity(ContentTypes.APPLICATION_JSON, scenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual "Was successfully updated scenario"
      }
    }

    "PUT. Create new scenario. Unknown specId" in {
      Put("/sm/" + someWrongId + "/" + scenarioId)
        .withEntity(ContentTypes.APPLICATION_JSON, scenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"Unknown specId : $someWrongId"
      }
    }
    "PUT. Create new scenario. There already was Scenario with such id" in {
      Put("/sm/" + specId + "/" + scenarioId)
        .withEntity(ContentTypes.APPLICATION_JSON, scenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"There already was Scenario with such id : $scenarioId"
      }
    }
    "PUT. Create new scenario. Was successfully persisted spec with new scenario" in {
      Put("/sm/" + specId + "/someNewID")
        .withEntity(ContentTypes.APPLICATION_JSON, scenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual "Was successfully persisted spec with new scenario"
      }
    }
    "PUT. Create new scenario. With empty fields" in {
      Put("/sm/" + specId + "/someNewID2")
        .withEntity(ContentTypes.APPLICATION_JSON, emptyScenario.asJson.toString) ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual "Was successfully persisted spec with new scenario"
      }
    }

    "DELETE. Unknown specId" in {
      Delete("/sm/" + someWrongId + "/" + scenarioId) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"Unknown specId : $someWrongId"
      }
    }
    "DELETE. Unknown scenarioId" in {
      Delete("/sm/" + specId + "/" + someWrongId) ~> controller.route ~> check {
        status.intValue() should be(400)
        responseAs[String] shouldEqual s"Unknown scenarioId (if you want create new scenario try PUT instead of DELETE) : $someWrongId"
      }
    }
    "DELETE. Was successfully removed scenario" in {
      Delete("/sm/" + specId + "/" + scenarioId) ~> controller.route ~> check {
        status.intValue() should be(200)
        responseAs[String] shouldEqual "Was successfully removed scenario"
      }
    }

  }

}
