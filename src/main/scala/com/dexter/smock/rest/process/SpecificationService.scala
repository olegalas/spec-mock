package com.dexter.smock.rest.process

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpResponse
import com.dexter.smock.db.SMRedisClient
import com.dexter.smock.model._
import com.dexter.smock.model.{QuickMockSpecification => Spec}
import com.dexter.smock.rest.ResponseMaker
import org.slf4j.{Logger, LoggerFactory}
import io.circe.syntax._
import io.circe.parser.decode

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class SpecificationService(redis: SMRedisClient)(implicit akkaSystem: ActorSystem) {

  val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  // register new specification (user)
  def > (specId: String, credentials: Credentials): Future[HttpResponse] = {
    (redis < specId) map {

      case Left(message) => ResponseMaker.internalErrorResponse(message)
      case Right(result) => result match {
        case Some(_) =>
          val message = s"There already was QuickMockSpecification by : $specId"
          LOG.warn(message)
          ResponseMaker.badResponse(message)
        case _ =>
          val spec = Spec(specId, credentials.userTokenId, Nil)
          redis > (specId, spec.asJson.toString)
          val message = "New spec was successfully persisted"
          LOG.info(s"$message : $spec")
          ResponseMaker.okResponse(message)
      }

    }
  }

  // save new scenario
  def > (specId: String, scenarioId: String, scenario: Scenario): Future[HttpResponse] = {
    (redis < specId) flatMap {

      case Left(message) => ResponseMaker.internalErrorResponseF(message)
      case Right(result) => result match {
        case Some(s) =>
          decode[Spec](s.utf8String).fold(Future.failed, Future.successful).map(spec => {

            if(spec.scenario.exists(sc => sc.scenarioId == scenarioId)) {
              val message = s"There already was Scenario with such id : $scenarioId"
              LOG.warn(message)
              ResponseMaker.badResponse(message)
            } else {
              val updatedSpec = spec.copy(scenario = scenario :: spec.scenario)
              redis > (specId, updatedSpec.asJson.toString)
              val message = "Was successfully persisted spec with new scenario"
              LOG.info(s"$message : $updatedSpec")
              ResponseMaker.okResponse(message)
            }

          })

        case _ =>
          val message = s"Unknown specId : $specId"
          LOG.warn(message)
          ResponseMaker.badResponseF(message)
      }

    }
  }

  // update specific scenario if exist
  def >> (specId: String, scenarioId: String, scenario: Scenario): Future[HttpResponse] = {
    (redis < specId) flatMap {

      case Left(message) => ResponseMaker.internalErrorResponseF(message)
      case Right(result) => result match {
        case Some(s) =>
          decode[Spec](s.utf8String).fold(Future.failed, Future.successful).map(spec => {

            if(spec.scenario.exists(sc => sc.scenarioId == scenarioId)) {
              val filtered = spec.scenario.filterNot(sc => sc.scenarioId == scenarioId)
              val updatedSpec = spec.copy(scenario = scenario :: filtered)
              redis > (specId, updatedSpec.asJson.toString)
              val message = "Was successfully updated scenario"
              LOG.info(s"$message : $updatedSpec")
              ResponseMaker.okResponse(message)
            } else {
              val message = s"Unknown scenarioId (if you want create new scenario try PUT instead of POST) : $scenarioId"
              LOG.warn(message)
              ResponseMaker.badResponse(message)
            }

          })

        case _ =>
          val message = s"Unknown specId : $specId"
          LOG.warn(message)
          ResponseMaker.badResponseF(message)
      }

    }
  }

  // get full spec
  def << (specId: String): Future[HttpResponse] = {
    (redis < specId) flatMap {

      case Left(message) => ResponseMaker.internalErrorResponseF(message)
      case Right(result) => result match {
        case Some(s) =>
          decode[Spec](s.utf8String).fold(Future.failed, Future.successful).map(spec => {
            LOG.info(s"Was found spec : $spec")
            ResponseMaker.specResponse(spec)
          })
        case _ =>
          val message = s"Unknown specId : $specId"
          LOG.warn(message)
          ResponseMaker.badResponseF(message)
      }

    }
  }

  // get specific scenario
  def << (specId: String, scenarioId: String): Future[HttpResponse] = {
    (redis < specId) flatMap {

      case Left(message) => ResponseMaker.internalErrorResponseF(message)
      case Right(result) => result match {
        case Some(s) =>
          decode[Spec](s.utf8String).fold(Future.failed, Future.successful).map(spec => {

            spec.scenario.find(sc => sc.scenarioId == scenarioId) match {
              case Some(scenario) =>
                LOG.info(s"Was found scenario : $scenario")
                ResponseMaker.scenarioResponse(scenario)
              case _ =>
                val message = s"Unknown scenarioId : $scenarioId"
                LOG.warn(message)
                ResponseMaker.badResponse(message)
            }
          })
        case _ =>
          val message = s"Unknown specId : $specId"
          LOG.warn(message)
          ResponseMaker.badResponseF(message)
      }

    }
  }

  // delete scenario
  def ! (specId: String, scenarioId: String): Future[HttpResponse] = {
    (redis < specId) flatMap {

      case Left(message) => ResponseMaker.internalErrorResponseF(message)
      case Right(result) => result match {
        case Some(s) =>
          decode[Spec](s.utf8String).fold(Future.failed, Future.successful).map(spec => {

            if(spec.scenario.exists(sc => sc.scenarioId == scenarioId)) {
              val filtered = spec.scenario.filterNot(sc => sc.scenarioId == scenarioId)
              val updatedSpec = spec.copy(scenario = filtered)
              redis > (specId, updatedSpec.asJson.toString)
              val message = "Was successfully removed scenario"
              LOG.info(s"$message : $updatedSpec")
              ResponseMaker.okResponse(message)
            } else {
              val message = s"Unknown scenarioId (if you want create new scenario try PUT instead of DELETE) : $scenarioId"
              LOG.warn(message)
              ResponseMaker.badResponse(message)
            }

          })

        case _ =>
          val message = s"Unknown specId : $specId"
          LOG.warn(message)
          ResponseMaker.badResponseF(message)
      }

    }
  }

}
