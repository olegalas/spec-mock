package com.dexter.smock.rest

import akka.http.scaladsl.model.{HttpEntity, HttpResponse, StatusCodes}
import akka.http.scaladsl.model.MediaTypes.`application/json`
import com.dexter.smock.model.{Scenario, QuickMockSpecification => Spec}
import io.circe.syntax._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object ResponseMaker {

  def specResponse(spec: Spec): HttpResponse = {
    HttpResponse(
      status = StatusCodes.OK,
      entity = HttpEntity(`application/json`, spec.asJson.toString)
    )
  }

  def scenarioResponse(scenario: Scenario): HttpResponse = {
    HttpResponse(
      status = StatusCodes.OK,
      entity = HttpEntity(`application/json`, scenario.asJson.toString)
    )
  }

  def badResponseF(message: String): Future[HttpResponse] = {
    Future {
      badResponse(message)
    }
  }

  def okResponseF(message: String): Future[HttpResponse] = {
    Future {
      okResponse(message)
    }
  }

  def internalErrorResponseF(message: String): Future[HttpResponse] = {
    Future {
      internalErrorResponse(message)
    }
  }

  def badResponse(message: String): HttpResponse = {
    HttpResponse(
      status = StatusCodes.BadRequest,
      entity = HttpEntity(message)
    )
  }

  def okResponse(message: String): HttpResponse = {
    HttpResponse(
      status = StatusCodes.OK,
      entity = HttpEntity(message)
    )
  }

  def internalErrorResponse(message: String): HttpResponse = {
    HttpResponse(
      status = StatusCodes.InternalServerError,
      entity = HttpEntity(message)
    )
  }

}
