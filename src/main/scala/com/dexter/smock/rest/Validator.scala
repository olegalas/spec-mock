package com.dexter.smock.rest

import akka.http.scaladsl.model.HttpResponse
import com.dexter.smock.model.{Scenario, QuickMockSpecification => Spec}

import scala.concurrent.Future

object Validator {

  def >>(spec: Spec): Either[Future[HttpResponse], Unit] = {
    spec.specId match {
      case null => Left(ResponseMaker.badResponseF("specId is null"))
      case "" => Left(ResponseMaker.badResponseF("specId is blank"))
      case _ =>
        val scenarioList = spec.scenario

        checkScenarios(scenarioList) match {
          case Right(_) => Right()
          case Left(message) => Left(ResponseMaker.badResponseF(message))
        }
    }
  }

  def >>(scenario: Scenario): Either[Future[HttpResponse], Unit] = {
    checkScenario(scenario) match {
      case Right(_) => Right()
      case Left(message) => Left(ResponseMaker.badResponseF(message))
    }
  }

  private def checkScenarios(scenarios: List[Scenario]): Either[String, Unit] = {
    scenarios match {
      case null => Left("null")
      case Nil => Left("Nil")
      case scenario :: tail =>
        checkScenario(scenario) match {
          case Right(_) => checkScenarios(tail) match {
            case Left("Nil") => Right()
            case l: Left[String, Unit] => l
            case _ => Right()
          }
          case l: Left[String, Unit] => l
        }
    }
  }

  private def checkScenario(scenario: Scenario): Either[String, Unit] = {
    scenario.scenarioId match {
      case null => Left("incorrect scenarioId")
      case "" => Left("incorrect scenarioId")
      case _ =>
        if(!allowedMethods.contains(scenario.method.toUpperCase))
          Left("incorrect method")
        else
          Right()
    }
  }

  val allowedMethods = Set("OPTIONS", "POST", "PUT", "GET", "DELETE")

}
