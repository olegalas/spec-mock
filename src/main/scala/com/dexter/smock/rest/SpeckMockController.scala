package com.dexter.smock.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.Directives.{as, complete, entity, get, path, post, _}
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.dexter.smock.db.SMRedisClient
import com.dexter.smock.model._
import com.dexter.smock.rest.process.SpecificationService
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.immutable._

class SpeckMockController(redis: SMRedisClient)(implicit akkaSystem: ActorSystem) {

  val LOG: Logger = LoggerFactory.getLogger(this.getClass)
  val service: SpecificationService = new SpecificationService(redis)

  lazy val corsSettings: CorsSettings =
    CorsSettings.defaultSettings.withAllowedMethods(Seq(OPTIONS, POST, PUT, GET, DELETE))

  def route: Route = {
    cors(corsSettings) {

      path("sm" / Segment) {
        specId => {

          // Get full spec
          get {

            entity(as[HttpRequest]) { req =>
              LOG.info(s"GET. All spec be specId : $specId")
              complete(service << specId)
            }

          // Register new spec
          } ~ put {

            entity(as[Credentials]) { credentials =>
              LOG.info(s"PUT. Create new SpecId : $specId ; credentials: $credentials")
              complete(service > (specId, credentials))
            }

          }

        }
      } ~ path("sm" / Segment / Segment) {
        (specId, scenarioId) => {

          // Get only scenario
          get {

            entity(as[HttpRequest]) { req =>
              LOG.info(s"GET. Scenario by specId : $specId ; scenarioId : $scenarioId")
              complete(service << (specId, scenarioId))
            }

          // Edit existed scenario
          } ~ post {

            entity(as[Scenario]) { scenario =>
              LOG.info(s"POST. Update scenario : $scenario ; by specId : $specId ; scenarioId : $scenarioId")
              val resp = Validator >> scenario match {
                case Right(_) => service >> (specId, scenarioId, scenario)
                case Left(badResp) => badResp
              }
              complete(resp)
            }

          // Create new scenario
          } ~ put {

            entity(as[Scenario]) { scenario =>
              LOG.info(s"PUT. Create new scenario : $scenario ; by specId : $specId ; scenarioId : $scenarioId")
              val resp = Validator >> scenario match {
                case Right(_) => service > (specId, scenarioId, scenario)
                case Left(badResp) => badResp
              }
              complete(resp)
            }

          // Delete existed scenario
          } ~ delete {
            entity(as[HttpRequest]) { req =>
              LOG.info(s"DELETE. SpecId - $specId ; ScenarioId : $scenarioId")
              complete(service ! (specId, scenarioId))
            }

          }
        }
      }
    }
  }

}
