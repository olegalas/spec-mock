package com.dexter.smock.db

import akka.actor.ActorSystem
import akka.util.ByteString
import org.slf4j.{Logger, LoggerFactory}
import redis.RedisClient

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

// default settings
class SMRedisClient(implicit akkaSystem: ActorSystem) {

  private final val LOG: Logger = LoggerFactory.getLogger(this.getClass)

  // default settings
  private val redis = RedisClient()

  private val futurePong: Future[String] = redis.ping()
  LOG.info("Ping to REDIS was sent!")
  futurePong.map(pong => {
    LOG.info(s"Redis replied with a $pong")
  })


  def > (key: String, value: String): Future[Either[String,Boolean]] = {
    redis.set(key, value).map(Right(_)).recover {
      case e: Exception =>
        redisError(e)
    }
  }

  def < (key: String): Future[Either[String, Option[ByteString]]] = {
    redis.get(key).map(Right(_)).recover {
      case e: Exception =>
        redisError(e)
    }
  }

  def ! (key: String*): Future[Either[String, Long]] = {
    redis.del(key :_*).map(Right(_)).recover {
      case e: Exception =>
        redisError(e)
    }
  }

  private def redisError(e: scala.Exception): Either[String, Nothing] = {
    LOG.error("Was exception during Redis request.", e)
    Left("Redis issue")
  }
}

