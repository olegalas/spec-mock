package com.dexter.smock

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.dexter.smock.db.SMRedisClient
import com.dexter.smock.rest.SpeckMockController
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContextExecutor

object Launch extends App {

  val LOG: Logger = LoggerFactory.getLogger(Launch.getClass)

  implicit val system: ActorSystem = ActorSystem("quick-mock-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val redis = new SMRedisClient
  val controller: SpeckMockController = new SpeckMockController(redis)

  val bindingFuture = Http().bindAndHandle(controller.route, "localhost", 8081)


  bindingFuture
    .map(binding => {
      LOG.info("Server was run")
      LOG.info(s"Address ${binding.localAddress}")
    }).recover {
    case t:Throwable =>
      LOG.error("There was an error during starting server", t)
      System.exit(-1)
  }

}
