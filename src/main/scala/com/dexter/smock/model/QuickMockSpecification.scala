package com.dexter.smock.model

import akka.http.scaladsl.model.ContentTypeRange
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.parser.decode
import io.circe.{Decoder, Encoder}

import scala.concurrent.Future

case class QuickMockSpecification(
                                   specId:      String,
                                   userTokenId: String,
                                   scenario:    List[Scenario]
                                 )
object QuickMockSpecification {

  private def jsonContentTypes: List[ContentTypeRange] = List(`application/json`)

  implicit val decodeSpec: Decoder[QuickMockSpecification] = deriveDecoder[QuickMockSpecification]
  implicit val encodeSpec: Encoder[QuickMockSpecification] = deriveEncoder[QuickMockSpecification]

  implicit val unmarshSpec: FromEntityUnmarshaller[QuickMockSpecification] = {
        Unmarshaller.stringUnmarshaller
          .forContentTypes(jsonContentTypes: _*)
          .flatMap { ctx => mat => json =>
              decode[QuickMockSpecification](json).fold(Future.failed, Future.successful)
            }
      }

}
case class Scenario(
                     scenarioId:    String,
                     method:        String,
                     segments:      List[String],
                     params:        Map[String, String],
                     headers:       Map[String, String],
                     body:          Option[String],
                     responseBody:  Option[String]
                   )
object Scenario {
  private def jsonContentTypes: List[ContentTypeRange] = List(`application/json`)
  implicit val decodeScenario: Decoder[Scenario] = deriveDecoder[Scenario]
  implicit val encodeScenario: Encoder[Scenario] = deriveEncoder[Scenario]

  implicit val unmarshScenario: FromEntityUnmarshaller[Scenario] = {
    Unmarshaller.stringUnmarshaller
      .forContentTypes(jsonContentTypes: _*)
      .flatMap { ctx => mat => json =>
        decode[Scenario](json).fold(Future.failed, Future.successful)
      }
  }
}
case class Credentials(userTokenId: String)
object Credentials {
  private def jsonContentTypes: List[ContentTypeRange] = List(`application/json`)
  implicit val decodeScenario: Decoder[Credentials] = deriveDecoder[Credentials]
  implicit val encodeScenario: Encoder[Credentials] = deriveEncoder[Credentials]

  implicit val unmarshScenario: FromEntityUnmarshaller[Credentials] = {
    Unmarshaller.stringUnmarshaller
      .forContentTypes(jsonContentTypes: _*)
      .flatMap { ctx => mat => json =>
        decode[Credentials](json).fold(Future.failed, Future.successful)
      }
  }
}