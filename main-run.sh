#!/usr/bin/env bash


sudo systemctl start nginx

redis-server & disown

java -jar ~/spec-mock/target/scala-2.12/spec-mock-0.0.1.jar -Xms300M -Xmx450M & disown

java -jar ~/quick-mock/target/scala-2.12/quick-mock-0.0.1.jar -Xms300M -Xmx450M & disown

java -jar ~/interface-mock-akka/target/scala-2.12/interface-mock-akka-0.0.1.jar -Xms300M -Xmx450M & disown

