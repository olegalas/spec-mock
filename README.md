# SPEC-MOCK

### RUN REDIS
> redis-server

### REST API (CRUD)
#####CREATE
PUT
> http://localhost:8081/sm/{specId}

body : 

```
{
  "specId": "dex",
  "scenario": [
    {
      "scenarioId": "dex",
      "method": "GET",
      "segments": [
        "seg1",
        "seg2"
      ],
      "params": {
        "param1": "val1",
        "param2": "val2"
      },
      "headers": {
        "header1": "val1",
        "header2": "val2"
      },
      "body": "body",
      "responseBody": "response"
    }
  ]
}
```

#####READ
GET
> http://localhost:8081/sm/{specId}

#####UPDATE
POST
> http://localhost:8081/sm/{specId}

body : 

```
{
  "specId": "dex",
  "scenario": [
    {
      "scenarioId": "dex",
      "method": "GET",
      "segments": [
        "seg1",
        "seg2"
      ],
      "params": {
        "param1": "val1",
        "param2": "val2"
      },
      "headers": {
        "header1": "val1",
        "header2": "val2"
      },
      "body": "body",
      "responseBody": "response"
    }
  ]
}
```

#####UPDATE
DELETE
> http://localhost:8081/sm/{specId}/{scenarioId}


### BUILD AND RUN
> sbt run