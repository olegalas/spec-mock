#!/bin/sh

exec scala "$0" "$@"
!#
object HelloWorld {
  def main(args: Array[String]) {

    import java.io.File
    import java.io.PrintWriter
    import scala.io.Source

    val f1 = args(0)
    val f2 = new File("build2.sbt")
    val w = new PrintWriter(f2)
    Source.fromFile(f1).getLines
      .map { x => {
        if(x.contains("version := ")) {
            val versionString = x.split(":=")(1).trim.replace("\"", "")
            println(s"current version is = $versionString")
            val lastNumberInVersion = new Integer(versionString.split("\\.")(2))
            val newVersion = x.substring(0, x.lastIndexOf('.')) + "." + (lastNumberInVersion + 1) + "\""
            println(s"new version is = ${newVersion.split(":=")(1).trim.replace("\"", "").mkString}")
            newVersion
        } else {
                x
            }
        }
      }
      .foreach(x => w.println(x))
    w.close()
    f2.renameTo(new File(f1))

  }
}
HelloWorld.main(args)